package wapidstyle.mm;

/**
 * Defines the height of a block.
 * @since 2b32612fd7928b22911e4baea8d3cadc7e9e15a4
 *
 */
public enum Height {
	VISUAL,
	CARPET,
	SLAB,
	FULL
}
