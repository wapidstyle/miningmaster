package wapidstyle.mm;

public class Mod {
	public static Mod[] mods = new Mod[100];
	
	public Mod mod;
	public String displayname;
	public String internalname;
	public int id;
	
	private static int curid = 1;
	private static int durid = -1;
	
	public static Mod init(String dn, String in){
		Mod mod1 = new Mod();
		mods[curid] = mod1;
		mod1.mod = mod1;
		mod1.displayname = dn;
		mod1.internalname = in;
		return mod1;
	}
	private Mod(){}
	
	public static void deinit(String in){
		boolean good = false;
		while(good == false){
			if(mods[durid].equals(in)){
				good = true;
			} else {
				durid = durid + 1;
			}
		}
	}
	public Mod getMod(){
		return mod;
	}
}
