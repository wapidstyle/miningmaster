package wapidstyle.mm;

import java.util.Random;

public class ModLoader {
	public String displayname;
	public String internalname;
	public int id;
	
	public static ModLoader[] modloaders = new ModLoader[100];
	
	public static ModLoader create(String d, String i){
		Random r = new Random(); // That constructor makes no sense.
		final int id = r.nextInt(100);
		ModLoader m = new ModLoader();
		m.displayname = d;
		m.internalname = i;
		modloaders[id] = m;
		m.id = id;
		r = null;
		return m;
	}
	public static void unload(ModLoaderInstance m, ModLoader l){
		m.deinit();
		modloaders[l.id] = null;
	}
}
