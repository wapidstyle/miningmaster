package wapidstyle.mm;

import wapidstyle.mm.error.ErrorWindow;

import java.util.Random;

public class Block {
	public Height height;
	public Integer id;
	public String displayname = "";
	public Mod mod;
	public Material material;
	
	static Block blocks[] = new Block[100];
	
	public static boolean occupied(int id){
		if(blocks[id] == null){
			return true;
		} else {
			return false;
		}
	}
	public static Block create(int id, String dn, Mod mod){
		Block b;
		if(!(occupied(id)== true) && !(id <= -1)){
			b = new Block();
			b.id = id;
			b.displayname = dn;
			blocks[id] = b;
		} else {
			b = new Block();
			b.id = id;
			ErrorWindow.showConflictWindow(b, mod);
		}
		return b;
	}
	public static Block create(String dn, Mod mod){
		Random r = new Random();
		return create(r.nextInt(50), dn, mod);
	}
	private Block(){}
}
