package wapidstyle.mm.render;

import wapidstyle.mm.render.block.BlockTexture;
import wapidstyle.mm.render.generic.ThreeDimensionalMesh;

public interface Renderer {
	public void createBlock(BlockTexture bt);
	public void createEntity(ThreeDimensionalMesh m);
	// TODO Add more stuff here.
}
