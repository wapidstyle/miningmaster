package wapidstyle.mm.error;

import wapidstyle.mm.Mod;
import wapidstyle.mm.Block;

import javax.swing.JOptionPane;

public class ErrorWindow {
	/*
	 * For testing purposes only. Do not use.
	 */
	public static void main(String args[]){
		final Mod fakeMod = Mod.init("Testing", "testingmod");
		final Block fakeBlock = Block.create(2, "BBQ SAUCE!", fakeMod);
		fakeMod.displayname = ":D";
		fakeBlock.displayname = "Hello World";
		showConflictWindow(fakeBlock, fakeMod);
	}
	public static void showConflictWindow(Block conflict, Mod mod){
		String message = "Error! A block known as " + conflict.displayname + " was attempted to be overriden by " + mod.displayname + "!";
		JOptionPane.showMessageDialog(null, message, "Error!", JOptionPane.ERROR_MESSAGE, null);
	}
	
}
