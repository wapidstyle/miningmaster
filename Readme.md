# MinerMaster [![Build Status](https://travis-ci.org/wapidstyle/MiningMaster.svg)](https://travis-ci.org/wapidstyle/MiningMaster)
Intended to be a flexible 'cousin', if you will, of Minecraft, MinerMaster is an experimental game intended to
be a counterpart to Minecraft, Terraria and other games of the same nature. Written in Java and built around
the API, the workings are intended to be basic yet expandable and modifiable. Encoded inside is the ability to
make mods, modloaders and more to expand your world. Literally.

More information about the features is to be on the [wiki](https://github.com/wapidstyle/MiningMaster/wiki).
